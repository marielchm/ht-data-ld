from model import vision_db
from decimal import Decimal
from pony.orm import *

class HT_table(vision_db.Entity):
    _table_ = 'HoldingTime'
    InternalID= PrimaryKey(str)
    Source = Required(str)
    Status=Optional(str)
    Counterparty =Optional(str)
    holding_time = Optional(Decimal)
    timestamp = Required(str)