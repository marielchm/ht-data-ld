import logging
import os

from pony.orm import *

sql_debug(False)
logging.root.setLevel(logging.NOTSET)
logging.basicConfig(level=logging.NOTSET, format='%(asctime)s : %(name)s : %(levelname)s : %(message)s')

logger = logging.getLogger(__name__)
DEBUG = os.getenv('DEBUG_MODE', False)

VISION_DB_HOST = os.environ['VISION_DB_HOST']
VISION_DB_PORT = os.environ['VISION_DB_PORT']
VISION_DB_NAME = os.environ['VISION_DB_NAME']
VISION_DB_USER = os.environ['VISION_DB_USER']
VISION_DB_PASSWORD = os.environ['VISION_DB_PASSWORD']
print(VISION_DB_PASSWORD)
PROCESS_ENABLE = os.environ['PROCESS_ENABLE']


logger.debug("PARAMETERS: ")
logger.debug("DEBUG: {DEBUG}".format(DEBUG=DEBUG))
logger.debug("PROCESS_ENABLE: {PROCESS_ENABLE}".format(PROCESS_ENABLE=PROCESS_ENABLE))
logger.debug("VISION_DB_HOST: {VISION_DB_HOST}".format(VISION_DB_HOST=VISION_DB_HOST))
logger.debug("VISION_DB_PORT: {VISION_DB_PORT}".format(VISION_DB_PORT=VISION_DB_PORT))
logger.debug("VISION_DB_NAME: {VISION_DB_NAME}".format(VISION_DB_NAME=VISION_DB_NAME))
logger.debug("VISION_DB_USER: {VISION_DB_USER}".format(VISION_DB_USER=VISION_DB_USER))
logger.debug("VISION_DB_PASSWORD: {VISION_DB_PASSWORD}".format(VISION_DB_PASSWORD="is configured " if VISION_DB_PASSWORD is not None else "is not configured"))



vision_db_params = dict(provider='mysql', user=VISION_DB_USER, password=VISION_DB_PASSWORD,
                         host=VISION_DB_HOST, database=VISION_DB_NAME, port=int(VISION_DB_PORT))



platforms = ['EdgeFX']
