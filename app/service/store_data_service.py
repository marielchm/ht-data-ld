import json
from datetime import datetime

from config import logger
from model.HoldingTime_table import HT_table
from pony.orm import db_session


class Save_data:
    @classmethod
    @db_session(serializable=True)
    def save_all(cls, data):
        if data.empty:
            logger.info("No data to update")
            pass
        else:
            json_objord = json.loads(data.to_json(orient='records'))
            for element in json_objord:
                if HT_table.exists(InternalID=element['InternalID']):
                    found = HT_table.get_for_update(InternalID=element['InternalID'])
                    # set all the values
                    found.set(InternalID = element['InternalID'],
                               Source = element['Source'],
                                Counterparty = element['Counterparty'],
                                Status = element['Status'],
                                holding_time = element['HT'],
                                timestamp = element['created'])

                else:
                    new = HT_table(InternalID = element['InternalID'],
                                Source = element['Source'],
                                Counterparty = element['Counterparty'],
                                Status = element['Status'],
                                holding_time = element['HT'],
                                timestamp = element['created'])
            logger.info("dataupdated ")

