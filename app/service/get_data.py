
import os
import pandas as pd
import sqlalchemy as db
import json
from config import *
import pytz
from datetime import datetime, timedelta


def get_unique_internal_IDS(start_date, end_date):
    dwh_db_name = VISION_DB_NAME
    dwh_db_user = VISION_DB_USER
    dwh_db_pass = VISION_DB_PASSWORD
    dwh_db_host = VISION_DB_HOST
    dwh_db_port = VISION_DB_PORT
    url = dwh_db_user + ':' + dwh_db_pass + '@' + dwh_db_host + ':' + dwh_db_port
    engine = db.create_engine(
        'mysql+pymysql://{url}/{database}?charset=utf8'.format(url=url, database=dwh_db_name))
    query_ids = """select InternalID ,max(Timestamp) Timestamp from ExternalOrders where
        Timestamp >= '{start_date}' and Timestamp <= '{end_date}' and TimeInForce in ('FOK','IOC') and
        Status IN ('Filled', 'Rejected', 'Canceled') group by InternalID""".format(
        start_date=start_date, end_date=end_date)
    query_status = """select InternalID, Timestamp, Status from ExternalOrders where
        Timestamp >= '{start_date}' and Timestamp <= '{end_date}' and TimeInForce in ('FOK','IOC')
         AND Status IN ('Filled', 'Rejected', 'Canceled')""".format(
        start_date=start_date, end_date=end_date)

    try:
        InternalIDs = pd.read_sql_query(query_ids, engine)
        status = pd.read_sql_query(query_status,engine)
        status = status.drop_duplicates()
    finally:
        engine.dispose()
    data = InternalIDs.merge(status, on=['InternalID','Timestamp'],how='left')
    return data


def get_holding_time(origin_id):
    dwh_db_name = VISION_DB_NAME
    dwh_db_user = VISION_DB_USER
    dwh_db_pass = VISION_DB_PASSWORD
    dwh_db_host = VISION_DB_HOST
    dwh_db_port = VISION_DB_PORT
    url = dwh_db_user + ':' + dwh_db_pass + '@' + dwh_db_host + ':' + dwh_db_port
    engine = db.create_engine(
        'mysql+pymysql://{url}/{database}?charset=utf8'.format(url=url, database=dwh_db_name))
    query = """SELECT InternalID,Source,Counterparty,min(Timestamp) created,max(Timestamp) executed FROM backup.ExternalOrders where InternalID in ('{origin_id}') group by InternalID """.format(origin_id=origin_id)
    try:
        data = pd.read_sql_query(query, engine)
        data['HT']=data['executed']-data['created']

    finally:
        engine.dispose()
    return data


def get_data(start_date,end_date):
    """
    timeZone = pytz.timezone("America/New_York")
    def is_dst(dt, timeZone):
        aware_dt = timeZone.localize(dt)
        return aware_dt.dst() != timedelta(0, 0)
    start_date = datetime.strftime(datetime.now() - timedelta(days=1), '%Y-%m-%d') + ' 21:00' if (
                is_dst(datetime.today(), timeZone) == True) else ' 22:00'
    end_date = datetime.strftime(datetime.now(), '%Y-%m-%d') + ' 21:00' if (
                is_dst(datetime.today(), timeZone) == True) else ' 22:00'
    trade_date = datetime.strftime(datetime.now(), '%Y-%m-%d')
    print(start_date)
    print(end_date) """
    timeZone = pytz.timezone("America/New_York")
    def is_dst(dt, timeZone):
        aware_dt = timeZone.localize(dt)
        return aware_dt.dst() != timedelta(0, 0)
    """
    start_date = datetime.strftime(datetime.now() - timedelta(days=1), '%Y-%m-%d') + ' 21:00' if (
                is_dst(datetime.today(), timeZone) == True) else ' 22:00'
    end_date = datetime.strftime(datetime.now(), '%Y-%m-%d') + ' 21:00' if (
                is_dst(datetime.today(), timeZone) == True) else ' 22:00'"""

    start_date = datetime.strptime(start_date, '%Y-%m-%d')
    end_date = datetime.strptime(end_date, '%Y-%m-%d')
    start_date = datetime.strftime(start_date,'%Y-%m-%d') + ' 21:00' if (
            is_dst(start_date, timeZone) == True) else datetime.strftime(start_date,'%Y-%m-%d') +' 22:00'
    end_date = datetime.strftime(end_date, '%Y-%m-%d') + ' 21:00' if (
            is_dst(end_date, timeZone) == True) else datetime.strftime(end_date, '%Y-%m-%d') +' 22:00'
    trades = get_unique_internal_IDS(start_date, end_date)
    if not trades.empty:
        Origin_list = list(trades.InternalID.unique())
        Origin_list = "','".join(map(str, Origin_list))
        holding_time = get_holding_time(Origin_list)
        data_ht = trades.merge(holding_time, how='left', on='InternalID')
        data_ht['created'] = data_ht['created'].dt.strftime('%Y-%m-%d %H:%M:%S.%f')
    else:
        data_ht=pd.DataFrame()
    return data_ht