import datetime
import sched
import time
import pandas as pd
from config import logger
from model.HoldingTime_table import HT_table
from service.store_data_service import Save_data
from service.get_data import *
import pytz
import schedule
from datetime import datetime, timedelta

class Schedule:
    @classmethod
    def store_data(cls):
        process_job()
        schedule.every(40).minutes.do(process_job)
        while True:
         schedule.run_pending()
         time.sleep(1)


def process_job():
    if datetime.now(pytz.timezone('UTC')).hour >= 22:
        f_today = datetime.now(pytz.timezone('UTC')) + timedelta(days=1)
    else:
        f_today = datetime.now(pytz.timezone('UTC'))
    start_date = f_today - timedelta(days=1)
    end_date = f_today
    start_date = start_date.strftime('%Y-%m-%d')
    end_date = end_date.strftime('%Y-%m-%d')
    """for i in range(0, 40):
        start_date = datetime.strptime('2021-08-26 22:00:00', '%Y-%m-%d %H:%M:%S') + timedelta(days=i)
        end_date = start_date + timedelta(days=1)"""
    logger.info(start_date)
    logger.info(end_date)
    logger.info("Getting data ")
    data = get_data(start_date,end_date)
    logger.info("Getting data ")
    if not data.empty:
        Save_data.save_all(data)
        logger.info("data_stored")
    else:
        logger.info("There is no data to update")

