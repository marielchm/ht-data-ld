from config import *
from model import *
from scheduler.scheduler import *


vision_db.bind(**vision_db_params)
vision_db.generate_mapping(check_tables=True)


def main():
    if PROCESS_ENABLE == 'True':
        logger.info("Process is enabled")
        Schedule.store_data()
    else:
        logger.info("Process is disabled")


if __name__ == '__main__':
    main()
