#!/bin/bash
FILE_NAME=docker-compose.yml
if [[ $1 = "dev" ]]
then
    FILE_NAME=docker-compose.$1.yml
fi
docker-compose -f ${FILE_NAME} down
